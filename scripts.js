  // Functions and variables

  const punctuation = /[!"#$%&'()*+,-./:;<=>?@[\]^_`{|}~]/g;
  var inputQuery;
  var titleMatches;

  // Get a list of title objects that correspond to the input query and handle appropriately depending on how many there are
  function parseQuery() { 

    titleMatches = $.grep(titleResults,function(item){
      return item.primaryTitle.toLowerCase().replace(punctuation,"").includes(inputQuery.toLowerCase().replace(punctuation,""))
    });
    
    switch (titleMatches.length) {
      case 0:
      noMatch();
      break;
    
      case 1:
      singleMatch();
      break;
    
      case 2:
      multiMatch();
      break;
    }
  };

  function noMatch() {

    $("#output").html("Sorry, no results");
    $("#headerBasic").html("IMDB rating search");
    $("#headerLink").html("");
  
    // Refocus the search bar
    $("#titleField").focus();

  };

  function singleMatch() {

    var title = titleMatches[0]
    var titleId = title.tconst;
    var titleFull = title.primaryTitle+" ("+title.startYear+")"
    var titleType = title.titleType;
    
    if (titleType == "tvSeries") { // Check if it's a TV show, if not then go to fallback
    
    // Make an array of episodes by searching for all entries in episodeResults that have the correct parent tconst
    var episodeMatches = $.grep(episodeResults,function(item){
      return item.parentTconst == titleId
    });
    
    // For each episode, fetch the corresponding rating from ratingResults and append it to the episode object
    
    var nRows = 0;
    var nCols = 0;
    
    $(episodeMatches).each(function(index){
    
      var episode = episodeMatches[index];
      var episodeId = episode.tconst;
      var ratingMatches = $.grep(ratingResults,function(item){return item.tconst == episodeId});
      nRows = Math.max(nRows, episode.episodeNumber);
      nCols = Math.max(nCols, episode.seasonNumber);
    
      try { // in case some episodes don"t have ratings
      rating = ratingMatches[0].averageRating;
      } catch {
      rating = "-";
      }
    
      $.extend(episode,{"averageRating":rating});
    
    });
    
    // Populate episode table
    
    var ratingTable = "<table>\n<tr><th></th>"
    rangeCols = [...Array(nCols).keys()];
    
    // Making the header row
    $(rangeCols).each(function(index){
      colNum = index+1;
      ratingTable += "<th>"+colNum+"</th>";
    });
    
    ratingTable += "</tr>\n";
    
    // The episodes are not always ordered properly so we grep the rows one by one from the full list
    for (let row = 1; row < nRows; row++) {
    
      whichEpisodes = $.grep(episodeMatches,function(item){
      return item.episodeNumber == row+1;
      });
    
      whichEpisodes.sort((a, b) => (a.seasonNumber > b.seasonNumber) ? 1 : -1); 
    
      cells=[]

      $(whichEpisodes).each(function(index){
        cells.push(whichEpisodes[index].averageRating);
      });
    
      // filling out the row to match the others if needed
      for (let col = cells.length; col < nCols; col++) {
        cells.push("")
      }
      
      ratingTable += "<tr><th>"+row+"</th><td>"+cells.join("</td><td>")+"</td></tr>\n";
    }
    
    $("#output").html(ratingTable);
    
    } else { // if it"s not a TV show, try to fetch a single rating
    
      var ratingMatches = $.grep(ratingResults,function(item){
        return item.tconst == titleId
      });

      try {
      singleRating = ratingMatches[0].averageRating;
      } catch {
      singleRating = "-"
      }
    
    $("#output").html("Average rating: "+singleRating);
    
    };
    
    // Populate the header with the link to the IMDB page
    $("#headerBasic").html("Average ratings for ");
    $("#headerLink").html(titleFull);
    $("#headerLink").attr("href","https://www.imdb.com/title/"+titleId);
    
  };

  function multiMatch() {

    $("#output").html("Which one?");

    // Populate the list of results
    var listOptions = "";
  
    $(titleMatches).each(function(index){
  
      title = titleMatches[index];
      listOptions += "<p class=\"titleOption\" value=\""+title.tconst+"\">"+title.primaryTitle+" ("+title.startYear+")</p>\n"
  
    });
  
    // Deploy the list
    $("#divOptions").html(listOptions);
  
  };

  // Define an async function that takes a filepath and returns a papa parse promise for the parsed array of the corresponding .TSV
  const readTSV = async (filePath) => {
    const filepath = filePath
  
    return new Promise(resolve => {
      Papa.parse(filepath, {
        header: true,
        download: true, // this is what makes it download and read the file locally, and will ideally be superceded by reading a Node JS stream
        dynamicTyping: true,
        complete: results => {
          resolve(results.data);
        }
      });
    });
  };

$(document).ready(function() {

  // Initialise the promises for the three TSVs we"re using
const titlePromise = readTSV("data/titleBasics_trunc.tsv");
const episodePromise = readTSV("data/titleEpisode_trunc.tsv");
const ratingPromise = readTSV("data/titleRatings_trunc.tsv");

  // Combine all the promises, everything that follows is embedded in the .then() so that the rest of the code executes when they"re all done parsing
  Promise.all([titlePromise,episodePromise,ratingPromise]).then((values) => {

  // Unpack the values back into the individual arrays
    titleResults = values[0];
    episodeResults = values[1];
    ratingResults = values[2];

    // Pull user input from textbox
    $("#searchText").submit(function(input) {
    input.preventDefault();
    inputQuery=$("input").val();

    // In case someone decided to not pick between multiple, clear the list
    $("#divOptions").html("");

    // See how many matches there are
    parseQuery();

      // If there are multiple options to click on, this handles the click
      $("p.titleOption").click(function () {

        titleId=$(this).attr("value");

        titleMatches=$.grep(titleResults,function(item){
          return item.tconst == titleId
        });

        singleMatch();

        $("#divOptions").html(""); // clear the list
        
      });
    });
  });
});
